console.log("Hello World");


// [SECTION] JSON Objects
// JSON stands for JavaScript Object Notation
//"Parsing" and "Stingify"
// JSON is used for serializing different data type

//Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information



/*
Syntax:

{
	"propertyA" : valueA,
	"propertyB" : valueB
}


*/

//JSON Obect

// {
// 	"city" : "Quezon City",
// 	"province" : "Metro Manila",
// 	"country" : "Philippines"
// }


// JSON Array
// arrayName = cities

// "cities" : [
// 	{"city" : "Quezon City",
// 	"province" : "Metro Manila", 
// 	"country" : "Philippines"
// 	},

// 	{"city" : "Manila City",
// 	"province" : "Metro Manila", 
// 	"country" : "Philippines"
// 	},


// 	{"city" : "Makati City",
// 	"province" : "Metro Manila", 
// 	"country" : "Philippines"
// 	},

// ]

//[SECTION] JSON Methods
//the JSON object contains methods for parsing and converting data into stringified JSON

let batchesArr = [
{
	batchName: "Batch 217",
	schedule: "Full-Time"
},

{
	batchName: "Batch 218",
	schedule: "Part-Time"
}

]

//stringify --> converting method JS objects into a string

console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({

	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});

console.log(data)

[SECTION] Using Stringify with Variables

let firstName = prompt("What is your First Name?");
let lastName = prompt("What is your Last Name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city address belong to?")
};

let otherData = JSON.stringify({
	firstName : firstName,
	lastName : lastName,
	age : age,
	address : address
})

console.log(otherData);

[SECTION] Converting Stringified JSON into JS Objects

Parse Method

let batchesJSON = `[
{
	"batchName" : "Batch 217",
	"schedule" : "Full-Time"
},

{
	"batchName" : "Batch 218",
	"schedule" : "Part-Time"
}

]`

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));


let stringifiedObject = 
`{
	"name" : "John",
	"age" : 31,
	"address" : {
		"city" : "Manila",
		"country" : "Philippines"
	}
}`

console.log(JSON.parse(stringifiedObject));